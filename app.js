var express = require('express')
var hbs = require('express-hbs');

var app = express();

app.use(express.static(__dirname + '/public'));

app.engine('hbs', hbs.express4({
  partialsDir: __dirname + '/views/includes'
}));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views/pages');

app.get('/', function (req, res) {
  res.render('home', {
    title: 'title'
  });
});

app.get('/product', function (req, res) {
  res.render('product');
});

app.listen(3000);

/**
 * Slider home
 */

(function() {
  var hasSlider = document.querySelector('.banner-slider');

  if (!!hasSlider) {
    var swiper = new Swiper('.banner-slider .swiper-container', {
      navigation: {
        nextEl: '.banner-slider .swiper-button-next',
        prevEl: '.banner-slider .swiper-button-prev',
      },
      pagination: {
        el: '.banner-slider .swiper-pagination',
      },
      loop: true,
    });
  }
})();

/**
 * Open menu mobile
 */

(function() {
  document.querySelector('.open_menu').addEventListener('click', function (e) {
    e.preventDefault();
    document.querySelector('.navigation__mobile').classList.add('navigation__mobile--open');
    lockContent();
  })
})();

(function () {
  document.querySelector('.navigation__mobile-close').addEventListener('click', function (e) {
    e.preventDefault();
    document.querySelector('.navigation__mobile').classList.remove('navigation__mobile--open');
    lockContent();
  })
})();

/**
 * Scroll logo header
 */

(function() {
  var isHome = document.querySelector('.home');

  if (!!isHome) {
    document.addEventListener('scroll', function (e) {
      var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;

      if (scrollTop < 10) {
        document.querySelector('.header__logo').classList.remove('header__logo--small');
      } else {
        document.querySelector('.header__logo').classList.add('header__logo--small');
      }
    });
  }
})();

/**
 * Modal
 */

(function () {
  document.querySelector('.btn-create-account').addEventListener('click', function (e) {
    e.preventDefault();
    document.querySelector('.modal-app').classList.add('modal--open');
    lockContent();
  })
})();

(function () {
  var modalClose = document.querySelector('.modal__close');
  if (!!modalClose) {
    document.querySelector('.modal__close').addEventListener('click', function (e) {
      e.preventDefault();
      document.querySelector('.modal--open').classList.remove('modal--open');
      lockContent();
    });
  }
})();

/**
 * Helpers
 */

/**
 * lock content
 * usage: lockContent();
 */

function lockContent() {
  var body = document.querySelector('body'),
    isLockContent = body.classList.contains('lock_content');

  isLockContent ? body.classList.remove('lock_content') : body.classList.add('lock_content');
}

/**
 * Home actions
 */

(function() {
  var isHome = document.querySelector('.home');

  if (typeof appear !== 'function') return false;

  appear({
    elements: function elements() {
      return document.getElementsByClassName('bank__details');
    },
    appear: function appear(el) {
      var options = {
        useEasing: true,
        useGrouping: true,
        separator: ',',
        decimal: ',',
      };

      var count1 = new CountUp('count1', 0, 15.3, 1, 3, options);
      var count2 = new CountUp('count2', 0, 2.5, 1, 3, options);
      var count3 = new CountUp('count3', 0, 2.8, 1, 3, options);
      var count4 = new CountUp('count4', 0, 85, -1, 3, options);

      count1.start();
      count2.start();
      count3.start();
      count4.start();
    },
    bounds: -200,
  });

  appear({
    elements: function elements() {
      return document.getElementsByClassName('investments');
    },
    appear: function appear(el) {
      document.querySelector('.investments__counter.small').classList.add('investments__counter--size-1');
      document.querySelector('.investments__counter.big').classList.add('investments__counter--size-2');
    },
    bounds: -200,
  });
})();

/**
 * Product actions
 */

// toggle product type


// generic range
(function() {
  if(!document.querySelector('.simulator')) return false

  var inputSliderRange = document.querySelector('.simulator__step--2 .simulator__steps__range');
  var stepsValue = document.querySelector('.simulator__step--2 .simulator__steps__value');
  var initalRangeValue = 1000;

  rangeSlider.create(inputSliderRange, {
    polyfill: true,
    rangeClass: 'rangeSlider',
    fillClass: 'rangeSlider__fill',
    vertical: false,
    min: initalRangeValue,
    max: 250000,
    step: 1,
    borderRadius: 10,
    onInit: function() {
      stepsValue.innerHTML = (convertToBRL(initalRangeValue));
    },
    onSlide: function(position, value) {
      stepsValue.innerHTML = (convertToBRL(position));
    },
  });

  var rangeMonths = document.querySelector('.simulator__step--3 .simulator__steps__range');
  var stepsMonths = document.querySelector('.simulator__step--3 .simulator__steps__value');

  rangeSlider.create(rangeMonths, {
    polyfill: true,
    rangeClass: 'rangeSlider',
    fillClass: 'rangeSlider__fill',
    vertical: false,
    min: 0,
    max: 8000,
    step: 1000,
    borderRadius: 10,
    onInit: function() {
      stepsMonths.innerHTML = '3 Meses'
    },
    onSlide: function(position, value) {
      var month = '3 meses';
      switch (position) {
        case 1000:
          month = '6 meses'
          break;
        case 2000:
          month = '1 ano'
          break;
        case 3000:
          month = '1 ano e meio'
          break;
        case 3000:
          month = '2 anos'
          break;
        case 4000:
          month = '2 anos e meio'
          break;
        case 5000:
          month = '3 anos'
          break;
        case 6000:
          month = '3 anos e meio'
          break;
        case 7000:
          month = '4 anos'
          break;
        case 8000:
          month = '5 anos'
          break;
        default:
          month = '3 meses'
          break;
      }
      stepsMonths.innerHTML = month;
    },
  });
})();

function convertToBRL(value, isFullString = false) {
  return stringValue = parseFloat(value).toLocaleString('pt-BR', {
    style: 'currency',
    currency: 'BRL',
    minimumFractionDigits: 2
  })
}

/**
 * FAQ actions
 */

(function() {
  var hasAccordion = document.querySelector('.accordion');

  if (!!hasAccordion) {
    var accordionTitle = document.querySelectorAll('.accordion__title'),
      i = accordionTitle.length;

    while(i--) {
      accordionTitle[i].addEventListener('click', function(e) {
        e.preventDefault();
        this.parentElement.classList.toggle('accordion__group--open');
      });
    }
  }
})();

/**
 * Footer actions
 */
(function() {
  var footeNavigation = document.querySelector('.footer__navigation__title');

  footeNavigation.addEventListener('click', function (e) {
    e.preventDefault();
    this.parentElement.classList.toggle('footer__navigation--open');
  });
})();

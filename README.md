# BMG
## Executando o projeto

Para executar o projeto é necessário ter:

```
- Gulpjs
- Nodejs >= 7.0.0
- NPM
```

Caso necessário instale as dependências para rodar o projeto.

## Subindo o projeto

O projeto está estruturado de forma básica, sendo assim temos um arquivo HTML para cada página.

Obs: O projeto vem sem o repositório `dist` para visualizar os assets sem subir o projeto execute `npm run build`.

## Rodando versão de desenvolvimento

Para subir o projeto em versão de desenvolvimento é necessario executar o seguinte comando na raiz do projeto: `npm run watch`. Ele realizara as task's e terá um servidor com livereload para desenvolvimento.

## Gerando assets para deploy
Para gerar os arquivos de produção executar:
```npm run build```

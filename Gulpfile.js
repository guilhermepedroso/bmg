var
  gulp = require('gulp'),
  sass = require('gulp-sass'),
  del = require('del'),
  uglify = require('gulp-uglify'),
  imagemin = require('gulp-imagemin'),
  browserSync = require('browser-sync').create(),
  autoprefixer = require('gulp-autoprefixer'),
  runSequence = require('run-sequence');

/**
* Sass
*/

gulp.task('sass:dev', function() {
  return gulp.src('src/scss/**/*.scss')
  .pipe(sass({
    outputStyle: 'expanded'
  }).on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(gulp.dest('dist/css'))
  .pipe(browserSync.stream());
});

gulp.task('sass:prod', function() {
  return gulp.src('src/scss/**/*.scss')
  .pipe(sass({
    outputStyle: 'compressed'
  }).on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(gulp.dest('dist/css'))
});

/**
* Scripts
*/

gulp.task('scripts:dev', function() {
  gulp.src('src/js/*.js')
  .pipe(gulp.dest('dist/js'))
  .pipe(browserSync.stream());
});

// Scripts
gulp.task('scripts:prod', function() {
  gulp.src('src/js/*.js')
  .pipe(uglify())
  .pipe(gulp.dest('dist/js'))
  .pipe(browserSync.stream());
});

/**
* Images
*/

// Images
gulp.task('images', function() {
  gulp.src('src/images/**/*')
  .pipe(imagemin())
  .pipe(gulp.dest('dist/images'))
});

/**
* Fonts
*/

gulp.task('fonts', function() {
  gulp.src('src/fonts/**/*')
  .pipe(gulp.dest('dist/fonts/'))
});

/**
* Clean
*/

gulp.task('clean', function() {
  return del(['dist']);
});

/**
 * Dev task
 */

gulp.task('default', function() {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });

  gulp.start(['fonts', 'sass:dev', 'scripts:dev', 'images']);

  gulp.watch("src/scss/**/*.scss", ['sass:dev']);
  gulp.watch("src/js/*.js", ['scripts:dev']);
  gulp.watch("src/images/**/*", ['images']);
  gulp.watch("**/*.html").on('change', browserSync.reload);
});

/**
 * Prod task
 */

gulp.task('build', function(cb) {
  runSequence('clean', 'images', 'fonts', 'sass:prod', 'scripts:prod', cb);
});
